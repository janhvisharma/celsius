package sheridan;

public class Celsius {

	public static int fromFahrenheit(int temp) {
		int celsius = ((temp - 32) * 5) / 9;
		int celsiusRounded = (int) Math.round(celsius);
		return celsiusRounded;
	}
}
