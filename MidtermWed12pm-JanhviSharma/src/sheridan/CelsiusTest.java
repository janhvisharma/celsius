/*
 * @ author janhvi sharma
 * 991542217
 */
package sheridan;

import org.junit.Test;
import static org.junit.Assert.*;

public class CelsiusTest {
	
	@Test
	public void testCelsiusTempRegular() {
			
		int getCelsiustemp = Celsius.fromFahrenheit(104);
		assertTrue( "Invalid temperature provided", getCelsiustemp == 40 );
	}
	
	@Test(expected = Error.class)
	public void testCelsiusTempException() {
			
		int getCelsiustemp = Celsius.fromFahrenheit(-1);
		assertFalse( "Invalid temperature provided", getCelsiustemp == -18 );
	}
	
	@Test
	public void testCelsiusTempBoundaryIn() {
			
		int getCelsiustemp = Celsius.fromFahrenheit(34);
		assertTrue( "Invalid temperature provided", getCelsiustemp == 1 );
	}
	
	@Test(expected = Error.class)
	public void testCelsiusTempBoundaryOut() {
			
		int getCelsiustemp = Celsius.fromFahrenheit(35);
		assertFalse( "Invalid temperature provided", getCelsiustemp == 1 );
	}
}



